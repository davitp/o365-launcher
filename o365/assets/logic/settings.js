const saveOptions = () => {
	const hType = document.getElementsByName("type");
	const azure = document.getElementById("azure").checked;

	for (i = 0; i < hType.length; i++) {
		if (hType[i].checked) {
			linkType = hType[i].value;
			// azure = console.log(hType[i].value);
			chrome.storage.sync.set({ linkType: linkType, azure: azure }, () => {
				const status = document.getElementById("result");
				status.textContent = "🎉 Options saved.";
				setTimeout(() => {
					status.textContent = "";
				}, 1000);
			});
		}
	}
};

const restoreOptions = () => {
	chrome.storage.sync.get("linkType", function (result) {
		if (typeof result.linkType === "undefined") {
			// Set default value
			chrome.storage.sync.set({ linkType: "live" }, () => {});
		} else {
			const typeValue = result.linkType;
			document.getElementById(typeValue).checked = true;
		}
	});
	chrome.storage.sync.get("azure", function (result) {
		if (typeof result.azure === "undefined") {
			// Set default value
			chrome.storage.sync.set({ azure: false }, () => {});
			document.getElementById(azure).checked = false;
		} else {
			const azure = result.azure;
			document.getElementById("azure").checked = azure;
		}
	});
};

// chrome.storage.sync.remove("azure", function (result) {
// 	console.log("Removed key:", result);
// });

document.addEventListener("DOMContentLoaded", restoreOptions);
document.getElementById("save").addEventListener("click", saveOptions);

// document.addEventListener("DOMContentLoaded", restoreOptions);
// document.getElementById("save").addEventListener("click", saveOptions);
