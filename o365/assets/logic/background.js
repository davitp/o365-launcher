//	Name:			background.js
//	Description:	Notices on fresh installs and updates
//	Author:			Davit Peradze
//	Version:		2024-11-03
chrome.runtime.onInstalled.addListener(function (details) {
	if (details.reason === "install") {
		// Code to be executed on first install
	} else if (details.reason === "supdate") {
		// When extension is updated
	}
});
