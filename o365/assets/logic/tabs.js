const app_links = {
	mail: {
		office: "https://outlook.office.com/",
		live: "https://outlook.live.com/mail",
		keys: [],
	},

	calendar: {
		office: "https://outlook.office.com/calendar/",
		live: "https://outlook.live.com/calendar",
		keys: [],
	},

	drive: {
		office: "https://www.microsoft365.com/onedrive/",
		live: "https://onedrive.live.com/",
		keys: [],
	},

	word: {
		office: "https://www.microsoft365.com/launch/Word/",
		live: "https://www.microsoft365.com/launch/word",
		keys: ["doc", "docx"],
	},

	excel: {
		office: "https://www.microsoft365.com/launch/Excel",
		live: "https://www.microsoft365.com/launch/Excel",
		keys: ["xls", "xlsx"],
	},

	pp: {
		office: "https://www.microsoft365.com/launch/PowerPoint",
		live: "https://www.microsoft365.com/launch/PowerPoint",
		keys: ["ppt", "pptx"],
	},

	tasks: {
		office: "https://to-do.office.com/",
		live: "https://to-do.live.com/tasks/",
		keys: [],
	},

	azure: {
		office: "https://dev.azure.com/",
		live: "https://dev.azure.com/",
		keys: [],
	},
};

var typeValue = "";
chrome.storage.sync.get("linkType", function (result) {
	if (typeof result.linkType === "undefined") {
		// Set default value
		chrome.storage.sync.set({ linkType: "live" }, () => {
			typeValue = "live";
		});
	} else {
		typeValue = result.linkType;
	}
});

var showAzure = false;
var azureLink = document.getElementById("azure");
chrome.storage.sync.get("azure", function (result) {
	if (typeof result.azure === "undefined") {
		// Set default value
		chrome.storage.sync.set({ azure: false }, () => {});
		showAzure = false;
		azureLink.remove();
	} else {
		const azure = result.azure;
		if (azure) {
		} else {
			azureLink.remove();
		}
	}
});

const open_apps = document.querySelectorAll("[data-action]");
open_apps.forEach((app) => {
	app.addEventListener("click", (event) => {
		let app_url = app.dataset.app;

		console.log("APP:" + app_url);

		getFilteredTabs(app_url)
			.then((filteredTabs) => {
				filteredTabs.forEach((tab) => {
					console.log(tab.url);
				});
				if (filteredTabs.length == 0) {
					chrome.tabs.create({
						url: app_links[app_url][typeValue],
					});
				} else {
					switchToTab(filteredTabs[0]["id"]);
				}

				console.log(filteredTabs);
			})
			.catch((error) => {
				console.error(error);
			});
	});
});

function switchToTab(tabId) {
	chrome.tabs.update(tabId, { active: true });
	window.close();
}

function getFilteredTabs(filterKey) {
	filterKeys = [app_links[filterKey][typeValue]].concat(
		app_links[filterKey]["keys"]
	);
	console.log(filterKeys);
	return new Promise((resolve) => {
		chrome.tabs.query({}, (tabs) => {
			const filteredTabs = tabs.filter((tab) => {
				for (const filterKey of filterKeys) {
					if (tab.url.indexOf(filterKey) !== -1) {
						return true;
					}
				}
				return false;
			});
			resolve(filteredTabs);
		});
	});
}

document.querySelector("#go-to-options").addEventListener("click", function () {
	if (chrome.runtime.openOptionsPage) {
		chrome.runtime.openOptionsPage();
	} else {
		window.open(chrome.runtime.getURL("options.html"));
	}
});
